import click
import os



__program__ = "geolocate"
__version__ = '0.0.1'
__author__ = 'Carles Mateu <carlesm@carlesm.com>'
__copyright__ = 'Copyright (c) 2015  Carles Mateu '
__license__ = 'GPL3+'


import ConfigParser


class DataStore(object):
    """General Class storing data parameters common
    for all commands

    """

    defaults = {
        "store.driver" : "mongodb",
        "store.db" : "db",
        "store.collection" : "collection",
        "debug.level" : "0",
        "defaults.search" : "here",
    }

    def __init__(self, config_name):
        super(DataStore, self).__init__()
        # self.dbname = dbname
        # self.collection = collection
        self.config_name = config_name
        self.config = dict(self.defaults)
        self.read_config()

    def set_config(self, key, value):
        self.config[key] = value

    def read_config(self):
        # cfg = os.path.join(self.path, 'config.ini')
        parser = ConfigParser.RawConfigParser()
        parser.read([self.config_name])
        for section in parser.sections():
            for key, value in parser.items(section):
                self.set_config('%s.%s' % (section, key), value)

pass_ds = click.make_pass_decorator(DataStore)



@click.group()
# @click.option('--collection', default="coll",
#     help='MongoDB collection, defaults to: coll')
# @click.option('--dbname', default="db",
#     help='MongoDB dbname, defaults to: db')
@click.option('--config', default="./config.ini",
     help='File Path')
@click.pass_context
def cli(ctx, config):
    ctx.obj = DataStore(config)

@cli.command()
@pass_ds
def google_search(ds):
    click.echo('Google Search')
    #config = read_config()
    print ds.config

@cli.command()
def yahoo_search():
    click.echo('Yahoo Search')

@cli.command()
def multi_search():
    click.echo('Multiple Search')


if __name__ == '__main__':
    cli()
