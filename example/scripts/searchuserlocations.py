#!/usr/bin/env python
# -*- coding: utf-8 -*-
# vim: set fileencoding=utf-8 :

"""
SYNOPSIS

    searchuserlocations.py [-h,--help] [--version] dbname collection  [user ... ]

DESCRIPTION

    Selects from a mongodb (dbname) a collection (collection) locations for
    a user (optionally) or for all user


EXAMPLES

    dumpuserlocations.py  tweetsad user_tweets user

AUTHOR

    Carles Mateu <carlesm@carlesm.com>

LICENSE

    This script is published under the Gnu Public License GPL3+

VERSION

    0.0.2  $Id: dumpuserlocations.py 114 2012-05-14 16:44:14Z carlesm $
"""

import sys
import os
import traceback
import optparse
import time
import datetime
import re
import json
import pymongo
import geopy


__program__ = "searchuserlocations"
__version__ = '0.0.2'
__author__ = 'Carles Mateu <carlesm@carlesm.com>'
__copyright__ = 'Copyright (c) 2012  Carles Mateu '
__license__ = 'GPL3+'
__vcs_id__ = '$Id: searchuserlocations.py 114 2012-05-14 16:44:14Z carlesm $'


# Geocoding GOOGLE API
GOOGLE_KEY = ""


def setupdb(dbs, coll):
    global options, args, collection, insert
    connection = pymongo.Connection()
    db = connection[dbs]  # .tweets_ad
    collection = db[coll]  # .tweets
    if options.db:
        # print "options: ", options.db
        insert = db[options.db]
    else:
        insert = None


def setupgeo():
    global geo
    geo = geopy.geocoders.GoogleV3(GOOGLE_KEY)



def locfind(user, place):
    global geo
    if place is None:
        return (u"NOT FOUND", (0, 0))

    place = place.encode("utf-8")
    places = []
    stop = False
    count = 0
    justtried = False
    while not stop:
        try:
            if options.verbose:
                print "try: ", count,
            count += 1
            p = geo.geocode(place, exactly_one=False)
            if p:
                places = list(p)
                stop = True
                if options.verbose:
                    print "+"
            else:
                placename = u"NOT FOUND"
                (lat, lng) = (0, 0)
                # places.append((placename, (lat, lng)))
                locat = geopy.Location()
                places.append(locat) # (placename, (lat, lng)))
                stop = True
        except geopy.exc.GeocoderQueryError:
            if options.verbose:
                print "-"
            placename = u"NOT FOUND"
            (lat, lng) = (0, 0)
            locat = geopy.Location()
            places.append(locat) # (placename, (lat, lng)))
            stop = True
        # except geopy.geocoders.google.GTooManyQueries Error:
        except geopy.exc.GeocoderQuotaExceeded:
            if options.verbose:
                print "T"
            if count < 10:
                time.sleep(5)
                stop = False
            else:
                print "S"
                time.sleep(7200)
        # except:
        #     placename = u"OUT OF LIMIT"
        #     if options.verbose:
        #         print "*"
        #     (lat, lng) = (0, 0)
        #     places.append((placename, (lat, lng)))
        #     if count > 100:
        #         stop = True
    # print "***", places, "***"
    return places


def add_to_db(user, place, loc):
    global options, args, insert
    print "STORING",
    print loc
    obj = {}

    obj["user"] = user
    obj["place"] = place
    obj["locations"] = loc

    try:
        insert.insert(obj)
        print >> sys.stderr, ".",
    except:
        print >> sys.stderr, "*",

    # print json.dumps(obj, sort_keys=True, indent=4)

    pass


def save_to_db(screen_name, place, loc):
    global options, args, insert
    # print "STORING:"
    # print loc[0].address
    # print loc[0].latitude
    # print loc[0].longitude
    # print loc[0].raw

    for l in loc:
        user = collection.find_one(
            {"screen_name": screen_name})

        # print type(l)
        # print l.latitude
        # print l.address
        user["geolocation"] = {
            "address": l.address,
            "latitude": l.latitude,
            "longitude": l.longitude,
            "raw": l.raw}
        user["done"] = "NO"

        user = collection.update(
            {"screen_name": screen_name}, user)


def save_to_csv(user, loc):
    pass


def do_work(user, place, loc):
    global options, args, collection
    if options.db is not None:
        save_to_db(user, place, loc)

    if options.csv is not None:
        save_to_csv(user, loc)


def mainloop():
    global options, args, collection

    if len(args) > 2:
        userplaces = {}
        for u in args[2:]:
            userplaces[u] = {}    # = { args[2] : {} }
    else:
        userplaces = {}
        users = collection.find(
            {"geolocation" : { "$exists": False }}
            ).distinct('screen_name')
        for user in users:
            userplaces[user] = {}
        # print "looking for ", users

    for i in userplaces.keys():
        if options.verbose:
            print >>sys.stderr, "Asking for ", i
        twts = collection.find(
            {"screen_name": i}).distinct("location")
        if twts is not None:
            if options.verbose:
                print >>sys.stderr, "Locations found:",
            for t in twts:
                if t is not None:
                    if options.verbose:
                        print >>sys.stderr, t.encode("utf-8"),
                    loc = locfind(i, t)
                    do_work(i, t, loc)
            if options.verbose:
                print >>sys.stderr
        else:
            if options.verbose:
                print >>sys.stderr, "No locations found"


def main():
    global options, args
    setupdb(args[0], args[1])
    setupgeo()
    mainloop()
    print

if __name__ == '__main__':
    try:
        start_time = time.time()
        # usage = "usage: %prog [options] file dbname collection"
        parser = optparse.OptionParser(formatter=optparse.TitledHelpFormatter(),
                                       usage=globals()["__doc__"],
                                       version=__version__)
        parser.add_option('-v', '--verbose', action='store_true',
                          default=False, help='verbose output')
        parser.add_option('-j', '--json', action='store_true', default=False,
                          help='Single line JSON output')
        parser.add_option('-l', '--line-json', action='store_true',
                          default=False, help='Multiline JSON output')
        parser.add_option('-c', '--csv', action='store_true', default=False,
                          help='CSV Output (delimiter default |)')
        parser.add_option('-d', '--db', action='store',
                          help='Store in DB, collection specificed')
        (options, args) = parser.parse_args()

        if len(args) < 2:
            parser.error('missing argument(s), use --help for help')

        if options.verbose:
            print time.asctime()

        main()

        now_time = time.time()
        if options.verbose:
            print time.asctime()
        if options.verbose:
            print 'TOTAL TIME:', (now_time - start_time), "(seconds)"
        if options.verbose:
            print '          :', datetime.timedelta(seconds=(now_time - start_time))
        sys.exit(0)
    except KeyboardInterrupt, e:  # Ctrl-C
        raise e
    except SystemExit, e:  # sys.exit()
        raise e
    except Exception, e:
        print 'ERROR, UNEXPECTED EXCEPTION'
        print str(e)
        traceback.print_exc()
        os._exit(1)
