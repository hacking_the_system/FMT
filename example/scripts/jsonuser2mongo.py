#!/usr/bin/python 
 
import sys 
import json 
import pymongo 
 
 
connection = pymongo.Connection() 
db = connection.tweets_cat 
tweets = db.tweets 
users = db.users 
 
weirdencodes = [] 
 
 
def insert_mongo(a): 
    users.insert(a) 
    print " + " 
    pass 
 
 
def print_mongo2(a): 
    for i in a: 
        if i == "place": 
            for j in a[i]: 
                if j == "country_code": 
                    cc = a[i][j] 
                    if cc == "AD": 
                        # print json.dumps(a, sort_keys=True, indent=4) 
                        print a["user"]["screen_name"], 
                        try: 
                            # , " -> ", a["text"] 
                            print " -> ", a["user"]["location"] 
                        except UnicodeEncodeError: 
                            print "****" 
                            # if a["user"]["location"] not in weirdencodes: 
                            weirdencodes.append(a["user"]["location"]) 
 
 
def print_mongo(a): 
    if "place" in a: 
        if a["place"] is not None and "country_code" in a["place"]: 
            country = a["place"]["country_code"] 
            if country == "ES" or country == "AD": 
                # print json.dumps(a, sort_keys=True, indent=4) 
                print a["user"]["screen_name"], 
                try: 
                    print " -> ", a["user"]["location"]  # , " -> ", a["text"] 
                except UnicodeEncodeError: 
                    print "****" 
                    # if a["user"]["location"] not in weirdencodes: 
                    weirdencodes.append(a["user"]["location"]) 
 
 
def get_user_location(a): 
    userloc = {} 
    if "place" in a: 
        if a["place"] is not None and "country_code" in a["place"]: 
            country = a["place"]["country_code"] 
            if country == "ES" or country == "AD": 
                # print json.dumps(a, sort_keys=True, indent=4) 
                try: 
                    # print a["user"]["screen_name"], 
                    # print " -> ", a["user"]["location"]  # , " -> ", 
                    # a["text"] 
                    userloc["screen_name"] = a["user"]["screen_name"] 
                    userloc["location"] = a["user"]["location"] 
                    userloc["foreign"] = "UNK" 
#                   print userloc 
                except UnicodeEncodeError: 
                    print "****" 
                    # if a["user"]["location"] not in weirdencodes: 
                    weirdencodes.append(a["user"]["location"]) 
    return userloc 
 
 
def unique_insert(a): 
    try: 
        found = users.find_one({"screen_name": a["screen_name"]}) 
        if found is None: 
            users.insert(a) 
            print "." 
        else: 
            print "+" 
    except: 
        print " NO " 
 
 
def search_previous(a): 
    found = users.find_one({"screen_name": a["screen_name"]}) 
    print found 
 
 
f = open(sys.argv[1]) 
 
for line in f: 
    line = line.strip() 
    if line.startswith("{") and line.endswith("}"): 
        lastline = line 
        a = json.loads(line) 
        userloc = get_user_location(a) 
        if "screen_name" in userloc: 
            #print userloc["screen_name"] 
            unique_insert(userloc) 
        else: 
            #print "*****", userloc 
            pass 
        # check_insert(a) 
 
    else: 
        if line.startswith("{"): 
            pass 
 
# print weirdencodes 
# print json.dumps(a, sort_keys=True, indent=4) 
 
 
# print "**" 
# print weirdencodes 
    # print "*--> ", repr(i) 