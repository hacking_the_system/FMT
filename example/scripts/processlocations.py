#!/usr/bin/env python
# -*- coding: utf-8 -*-
# vim: set fileencoding=utf-8 :

"""
SYNOPSIS

    processlocations.py [-h,--help] [--version] dbname collection

DESCRIPTION

    Selects from a mongodb (dbname) a collection (collection) of locations
    and updates foreign (alien) key accordingly


EXAMPLES

    processlocations.py  tweetsad user_tweets

AUTHOR

    Carles Mateu <carlesm@carlesm.com>

LICENSE

    This script is published under the Gnu Public License GPL3+

VERSION

    0.0.1
"""
import sys
import os
import traceback
import optparse
import time
import datetime
import json
import pymongo
import re
from bson.objectid import ObjectId

def _byid(id):
    return ObjectId(id)


__program__ = "processlocations"
__version__ = '0.0.1'
__author__ = 'Carles Mateu <carlesm@carlesm.com>'
__copyright__ = 'Copyright (c) 2015  Carles Mateu '
__license__ = 'GPL3+'


def setupdb(dbs, coll):
    global options, args, collection
    connection = pymongo.Connection()
    db = connection[dbs]  # .tweets_ad
    collection = db[coll]  # .tweets


def mainloop():
    global options, args, collection

    users = collection.find(
                {"foreign" : "UNK"}
            ) #.distinct('screen_name')

    for user in users:
        # user = users[i]
        # print user["screen_name"],
        if len(user["geolocation"]["address"]):
            if re.match(".*Spain",user["geolocation"]["address"]):
                user["foreign"]="NO"
                # print "NO"
            else:
                user["foreign"]="YES"
                # print "YES"
        else:
            pass # print "UNK"
        user["done"]="YES"
        # print user
        collection.update( {"screen_name": user["screen_name"]},user)



def cacalavaca():
    global options, args, collection

    if len(args) > 2:
        userplaces = {}
        for u in args[2:]:
            userplaces[u] = {}    # = { args[2] : {} }
    else:
        userplaces = {}
        users = collection.find(
            {"geolocation" : { "$exists": False }}
            ).distinct('screen_name')

        for user in users:
            userplaces[user] = {}
        # print "looking for ", users



    for i in userplaces.keys():
        if options.verbose:
            print >>sys.stderr, "Asking for ", i
        twts = collection.find(
            {"screen_name": i}).distinct("location")
        if twts is not None:
            if options.verbose:
                print >>sys.stderr, "Locations found:",
            for t in twts:
                if t is not None:
                    if options.verbose:
                        print >>sys.stderr, t.encode("utf-8"),
                    loc = locfind(i, t)
                    do_work(i, t, loc)
            if options.verbose:
                print >>sys.stderr
        else:
            if options.verbose:
                print >>sys.stderr, "No locations found"




def main():
    global options, args
    setupdb(args[0], args[1])
    mainloop()
    print


if __name__ == '__main__':
    try:
        start_time = time.time()
        # usage = "usage: %prog [options] file dbname collection"
        parser = optparse.OptionParser(
                                formatter=optparse.TitledHelpFormatter(),
                                usage=globals()["__doc__"],
                                version=__version__)
        parser.add_option('-v', '--verbose', action='store_true',
                          default=False, help='verbose output')
        (options, args) = parser.parse_args()

        if len(args) < 2:
            parser.error('missing argument(s), use --help for help')

        if options.verbose:
            print time.asctime()

        main()

        now_time = time.time()
        if options.verbose:
            print time.asctime()
        if options.verbose:
            print 'TOTAL TIME:', (now_time - start_time), "(seconds)"
        if options.verbose:
            print '          :', datetime.timedelta(
                                            seconds=(now_time - start_time))
        sys.exit(0)
    except KeyboardInterrupt, e:  # Ctrl-C
        raise e
    except SystemExit, e:  # sys.exit()
        raise e
    except Exception, e:
        print 'ERROR, UNEXPECTED EXCEPTION'
        print str(e)
        traceback.print_exc()
        os._exit(1)
