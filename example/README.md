
Experimental method:

	1- Retrieve tweets restricted to a geographical area
	2- Select tweeting users from those tweets
	3- Retrieve "location" from users profile
	4- Try to geocode "location"
	5- If successful:
		5.1 Retrieve "older" tweets
		5.2 Plot locations
		5.3 Save map



Stream /

  Retrieves (streaming API) from twitter


-> Scripts:

Load TWEETS into mongo
tweets_cat . db.tweets
python json2mongo.py ../test-data/cat68kblanc.json


Load USER (screen_name) into mongo
tweets_cat . db.users
python jsonuser2mongo.py ../test-data/cat68kblanc.json

Geolocate USERs

python searchuserlocations.py -d locs tweets_cat users
(updates db.users)

--> User record:
{u'_id': {u'$oid': u'550166633cd96d5f34c44972'},
 u'foreign': u'UNK',
 u'geolocation': {u'address': u'Barcelona, Barcelona, Spain',
                  u'latitude': 41.3850639,
                  u'longitude': 2.1734035,
                  u'raw': {u'address_components': [{u'long_name': u'Barcelona',
                                                    u'short_name': u'Barcelona',
                                                    u'types': [u'locality',
                                                               u'political']},
                                                   {u'long_name': u'Barcelona',
                                                    u'short_name': u'Barcelona',
                                                    u'types': [u'administrative_area_level_4',
                                                               u'political']},
                                                   {u'long_name': u'El Barcelon\xe8s',
                                                    u'short_name': u'El Barcelon\xe8s',
                                                    u'types': [u'administrative_area_level_3',
                                                               u'political']},
                                                   {u'long_name': u'Barcelona',
                                                    u'short_name': u'B',
                                                    u'types': [u'administrative_area_level_2',
                                                               u'political']},
                                                   {u'long_name': u'Catalonia',
                                                    u'short_name': u'CT',
                                                    u'types': [u'administrative_area_level_1',
                                                               u'political']},
                                                   {u'long_name': u'Spain',
                                                    u'short_name': u'ES',
                                                    u'types': [u'country',
                                                               u'political']}],
                           u'formatted_address': u'Barcelona, Barcelona, Spain',
                           u'geometry': {u'bounds': {u'northeast': {u'lat': 41.4695761,
                                                                    u'lng': 2.2280099},
                                                     u'southwest': {u'lat': 41.320004,
                                                                    u'lng': 2.0695258}},
                                         u'location': {u'lat': 41.3850639,
                                                       u'lng': 2.1734035},
                                         u'location_type': u'APPROXIMATE',
                                         u'viewport': {u'northeast': {u'lat': 41.4695761,
                                                                      u'lng': 2.2280099},
                                                       u'southwest': {u'lat': 41.320004,
                                                                      u'lng': 2.0695258}}},
                           u'place_id': u'ChIJ5TCOcRaYpBIRCmZHTz37sEQ',
                           u'types': [u'locality', u'political']}},
 u'location': u'Barcelona - Catalunya',
 u'screen_name': u'alfsilvan'}
