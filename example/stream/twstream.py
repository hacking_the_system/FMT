#import tweetstream 
import json 
import tweepy 
 
 
 
# Authentication details.  
# 
consumer_key = '' 
consumer_secret = '' 
 
 
access_token = '' 
access_token_secret = '' 
 
 
 
# CATALUNYA  
locations = [0.18,40.64,3.18,42.42] 
 
# IBERIAN PENINSULA 
# locations = [-9.81,36.03,-1.65, 43.63, -2.024, 37.28, 5.09, 43.50] 
 
 
# This is the listener, resposible for receiving data 
class StdOutListener(tweepy.StreamListener): 
    def on_data(self, data): 
        #decoded = json.loads(data) 
        #print '@%s: %s' % (decoded['user']['screen_name'], decoded['text'].encode('ascii', 'ignore')) 
        print data 
        return True 
 
    def on_error(self, status): 
        print status 
 
 
if __name__ == '__main__': 
    l = StdOutListener() 
    auth = tweepy.OAuthHandler(consumer_key, consumer_secret) 
    auth.set_access_token(access_token, access_token_secret) 
    stream = tweepy.Stream(auth, l) 
    #stream.filter(track=['programming']) 
    stream.filter(locations=locations) 