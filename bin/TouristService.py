from StoreService import StoreService
from TweetService import TweetService
from LocationService import LocationService

import datetime
from dateutil.relativedelta import relativedelta

class TouristService:
    def __init__(self):
        self.store_service = StoreService()
        self.tweet_service = TweetService()
        self.location_service = LocationService()

    def __get_tourist_destinations_by_touritst_user(self,user):
        tweets = self.tweet_service.get_tweets_by_screen_name(user["screen_name"])
        destinations = []
        visited_places = []
        first_tweet_time = tweets[0].created_at
        three_months_ago = first_tweet_time - relativedelta(months=+3)
        for tweet in tweets:
            if (
                (tweet.place is not None) 
                and (tweet.place.full_name not in visited_places)
                and tweet.created_at > three_months_ago
            ):
                location = self.location_service.get_tweet_location(tweet)
                if location is not None:
                    if (
                        location["catalunya"] == False 
                        and location["country_code"] == user["origin"]["country_code"]
                    ):
                        break
                    if location["location"] not in visited_places:
                        visited_places.append(location["location"])
                        destination = {}
                        destination["location"] = location
                        destination["time"] = int(tweet.created_at.strftime("%Y%m%d%H%M%S"))
                        destinations.append(destination)
        print("  destinations visited by tourist: "+str(destinations))
        return destinations

    def analyze_tourist(self,user):
        screen_name = user["screen_name"]
        if self.store_service.exist_tourist(screen_name):
            self.store_service.remove_tourist(screen_name)
        tourist = {}
        tourist["username"] = screen_name
        tourist["origin"] = user["origin"]
        destinations = self.__get_tourist_destinations_by_touritst_user(user)
        tourist["destinations"] = destinations
        self.store_service.store_tourist(tourist)
