import configparser
import googlemaps
import LocationObj
try:
    from urllib import quote_plus  # Python 2.X
except ImportError:
    from urllib.parse import quote_plus  # Python 3+import tweepy

class GoogleService:
    def __init__(self):
        config = configparser.ConfigParser()
        config.read("config.ini")
        self.credentials = config["Google_API"]

    def get_client(self):
        API_KEY = self.credentials["API_KEY"]
        gmaps = googlemaps.Client(key=API_KEY)
        return gmaps

    def create_loc_obj_from_data(self,location,data):
        loc_obj = LocationObj.LocationObj()
        loc_obj.set_google_data(location,data)
        return loc_obj.data

    def create_loc_obj_from_country_data(self,country,country_code,data):
        loc_obj = LocationObj.LocationObj()
        loc_obj.set_country_data(country,country_code,data)
        return loc_obj.data

    def safely_encode_location(self,location):
        utf8_loc = location.encode("utf-8")
        url_loc = quote_plus(utf8_loc)
        return url_loc

    def is_data_reliable(self,data):
        #Maybe city_hall is also good type, alos natural_feature, and premise, route, airport...
        if 'political' not in data["types"]:
            return False
        return True

    def is_country(self,address_components):
        for component in address_components:
            if component["types"][0] == 'country':
                return True
        return False

    def is_valid_geolocated_data(self,data,flexible=False):
        if (
            len(data) > 0 
            and (self.is_data_reliable(data[0]) or flexible)
            and self.is_country(data[0]["address_components"])
            and data[0]["formatted_address"] != ""
        ):
            return True
        print("  Invalid geolocated google_data:")
        print(data)
        return False

    def is_valid_location(self,location):
        if location == "":
            return False
        if location is None:
            return False
        return True

    def is_valid_user_location(self,data):
        if (
            len(data) > 0 
            and data[0]["place_id"] != 'ChIJi7xhMnjjQgwR7KNoB5Qs7KY'
        ):
            return True
        print("  User main location cannot be Spain:")
        print(data)
        return False
        
    def geolocate_location(self,location,validate_user=False,fleixble=False):
        if not self.is_valid_location(location):
            return None
        client = self.get_client()
        result = client.geocode(location)
        if validate_user:
            if not self.is_valid_user_location(result):
                return None
        if self.is_valid_geolocated_data(result,fleixble):
            return self.create_loc_obj_from_data(location,result[0])
        print(location)
        return None

    def is_valid_country_data(self,data):
        if (
            len(data) > 0 
            and data[0]["formatted_address"] != ""
        ):
            return True
        print("  Country data seems incorrect:")
        print(data)
        return False

    def geolocate_country(self,country,country_code):
        if not self.is_valid_location(country):
            return None
        client = self.get_client()
        result = client.geocode(country)
        if self.is_valid_geolocated_data(result):
            return self.create_loc_obj_from_country_data(country,country_code,result[0])
        print(country)
        return None