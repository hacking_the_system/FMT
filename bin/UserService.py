import StoreService
import TwitterService
import LocationService
from TweetService import TweetService

class UserService:
    def __init__(self):
        self.store_service = StoreService.StoreService()
        self.twitter_service = TwitterService.TwitterService()
        self.location_service = LocationService.LocationService()
        self.tweet_service = TweetService()

    def determine_user_origin_by_location_field(self,user):
        if self.location_service.exist_location(user.location):
            print("  User location already on the system.")
            return self.location_service.get_location(user.location)
        user_origin = None
        if user.profile_location is not None:
            print("  Valid twitter location.")
            tw_id = user.profile_location["id"]
            user_origin = self.location_service.get_user_location_by_twitter_id(tw_id)
        if user_origin is None:
            print("  Geolocating location.")
            user_origin = self.location_service.geolocate_user(user.location)
        return user_origin

    def determine_user_origin_by_language(self,language):
        print("  Trying to determine with language:")
        user_origin = None
        print("  User language: "+language)
        if language == 'ca':
            print("  User from catalonia.")
            user_origin = self.location_service.get_location_by_country_code('CAT')
        #TODO explore more options to determine location by object
        return user_origin

    def determine_user_origin_by_tweets(self,screen_name):
        print("  Trying to determine with tweets:")
        user_origin = None
        tweets = self.tweet_service.get_tweets_by_screen_name(screen_name)
        user_origin = self.location_service.get_location_by_tweets(tweets)
        return user_origin

    def determine_user_origin_by_user_data(self,user):
        # TODO Here we can explore other options like obtain the location of 
        # the friends and determine the user location by his friends locations
        print("  Trying to determine user origin with data")
        data_origin = self.determine_user_origin_by_language(user.lang)
        if data_origin is None:
            data_origin = self.determine_user_origin_by_tweets(user.screen_name)
        if data_origin is None:
            return None
        if user.location != "":
            user_origin = self.location_service.flexible_geolocate(user.location,data_origin)
            if user_origin is not None:
                return user_origin
        return data_origin


    def create_user_from_screen_name(self,screen_name):
        print("  Creating user.")
        user = self.twitter_service.get_user_by_screen_name(screen_name)
        self.store_service.store_plain_text_user(user._json)
        if user.location is not None:
            print("  user from: "+user.location)
            user_origin = self.determine_user_origin_by_location_field(user)
            data_determined = False
        if user_origin is None:
            user_origin = self.determine_user_origin_by_user_data(user)
            data_determined = True
        user_data = user._json
        user_data["origin"] = user_origin
        user_data["data_determined"] = data_determined
        return user_data


    def get_user_by_screen_name(self,screen_name):
        if self.store_service.exist_user(screen_name):
            print("  User already in the system.")
            return self.store_service.get_user(screen_name)
        user = self.create_user_from_screen_name(screen_name)
        if user is not None:
            self.store_service.store_user(user)
        return user
