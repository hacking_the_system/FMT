from StoreService import StoreService
from TwitterService import TwitterService

class TweetService:
    def __init__(self):
        self.store_service = StoreService()
        self.twitter_service = TwitterService()

    def get_tweets_by_screen_name(self,screen_name):
        return self.twitter_service.get_tweets_by_screen_name(screen_name)

    def store_tweet(self,data):
        self.store_service.store_tweet(data)