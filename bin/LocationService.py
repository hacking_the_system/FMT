from StoreService import StoreService
from TwitterService import TwitterService
from LocationObj import LocationObj
from GoogleService import GoogleService
import Utils
import pycountry

class LocationService:
    def __init__(self):
        self.store_service = StoreService()
        self.twitter_service = TwitterService()
        self.google_service = GoogleService()

    def get_tweet_location(self,tweet):
        if tweet.place is not None:
            print("  Debug tl: "+tweet.place.full_name)
            if self.store_service.exist_location(tweet.place.full_name):
                return self.store_service.get_location(tweet.place.full_name)
            location = self.get_location_by_twitter_id(tweet.place.id)
            if location is not None:
                location["location"] = tweet.place.full_name
                print("  Debug tl: "+location["location"])
                self.store_location(location)
                return location

    def __get_country_location_from_locations(self,locations):
        country_codes = {}
        for location in locations:
            if location["catalunya"]:
                country_code = 'CAT'
            else:
                country_code = location["country_code"]
            if country_code == "":
                continue
            if country_code in country_codes:
                country_codes[country_code] += 1
            else:
                country_codes[country_code] = 1
        print("  locations visited by user: "+str(country_codes))
        if len(country_codes) == 0:
            return None
        max_country_code = Utils.get_key_from_max_val(country_codes)
        return self.get_location_by_country_code(max_country_code)

    def get_location_by_tweets(self,tweets):
        locations = []
        for tweet in tweets:
            location = self.get_tweet_location(tweet)
            if location is not None:
                locations.append(location)
        return self.__get_country_location_from_locations(locations)

    def get_user_location_by_twitter_id(self,tw_id):
        if tw_id == "ecdce75d48b13b64":
            return None
        location = self.get_location_by_twitter_id(tw_id)
        if location is not None:
            if self.store_service.exist_location(location["location"]) == False:
                self.store_location(location)
        return location

    def get_location_by_country_code(self,country_code):
        mapping = {country.alpha_2: country.name for country in pycountry.countries}
        if country_code == 'CAT':
            country = 'Catalonia'
            country_code = 'ES'
        else:
            country = mapping.get(country_code)
        if self.store_service.exist_location(country):
            return self.store_service.get_location(country)
        location = self.google_service.geolocate_country(country,country_code)
        if location is not None:
            self.store_location(location)
        return location

    def get_location_by_twitter_id(self,tw_id):
        return self.twitter_service.get_location_from_twitter_id(tw_id)

    def get_location_by_stream_data(self,data):
        full_name = data["place"]["full_name"]
        print("  Debug: "+full_name)
        if self.store_service.exist_location(full_name):
            return self.store_service.get_location(full_name)
        tw_id = data["place"]["id"]
        location = self.get_location_by_twitter_id(tw_id)
        if location is not None:
            location["location"] = full_name
            print("  Debug: "+location["location"])
            self.store_service.store_location(location)
        return location

    def flexible_geolocate(self,location,data_origin):
        loc = self.google_service.geolocate_location(location,False,True)
        if loc is not None:
            if (
                loc["country_code"] == data_origin["country_code"]
                and loc["catalunya"] == data_origin["catalunya"]
            ):
                loc["location"] = location
                self.store_location(loc)
                return loc
        loc = self.twitter_service.geolocate(location)
        if loc is not None:
            if (
                loc["country_code"] == data_origin["country_code"]
                and loc["catalunya"] == data_origin["catalunya"]
            ):
                loc["location"] = location
                self.store_location(loc)
                return loc
        return None

    def store_location(self,location):
        self.store_service.store_location(location)

    def geolocate_user(self,location):
        return self.google_service.geolocate_location(location,True)

    def geolocate_location(self,location):
        return self.google_service.geolocate_location(location)

    def get_location(self,location):
        return self.store_service.get_location(location)        
    
    def exist_location(self,location):
        return self.store_service.exist_location(location)