from MongoDbService import MongoDbService
import plotly
import plotly.plotly as py
import plotly.graph_objs as go
import configparser
import pandas as pd
from bson.code import Code
from AquireUserInfo import AquireUserInfo
import sys
import os

class TourismInformationSystem:
    def __init__(self):
        config = configparser.ConfigParser()
        config.read("config.ini")
        parameters = config["Plotly"]
        plotly.tools.set_credentials_file(username=parameters["username"], api_key=parameters["API_KEY"])
        self.MAP_KEY = parameters["MAPBOX_KEY"]
        self.mongodb_service = MongoDbService()
        self.AUI = AquireUserInfo()
    
    def run(self):
        while True:
            os.system('clear')
            self.show_header()
            self.show_menu()
            inp = raw_input(">> ")
            try:
                choice = int(inp)
                if choice == 0:
                    break
                elif choice == 1:
                    self.data_analyzed()
                elif choice == 2:
                    self.touristic_distribution('percentage')
                elif choice == 3:
                    self.touristic_distribution('total')
                elif choice == 4:
                    self.touristic_distribution('procedence')
                elif choice == 5:
                    self.touristic_places()
                elif choice == 6:
                    self.touristic_places_top()
                elif choice == 7:
                    username = raw_input("\nEnter username:\n")
                    self.show_tourist_route(username)
                elif choice == 8:
                    country_code = raw_input("\nEnter 2 chars country code (ALL for global):\n")
                    self.touristic_routes(country_code)
            except:
                os.system('clear')
                print("Invalid option\n")
                raw_input("press any key to continue...")

    def show_header(self):
        header = """
 /$$$$$$$$ /$$$$$$  /$$$$$$ 
|__  $$__/|_  $$_/ /$$__  $$
   | $$     | $$  | $$  \__/
   | $$     | $$  |  $$$$$$ 
   | $$     | $$   \____  $$
   | $$     | $$   /$$  \ $$
   | $$    /$$$$$$|  $$$$$$/
   |__/   |______/ \______/ 
                        
Tourism Information System
        """                                                                                                             
        print(header)

    def show_menu(self):
        print("Select an option to display:\n")
        print("\t[1] Data analyzed")
        print("\t[2] Touristic distribution (percentage)")
        print("\t[3] Touristic distribution (total)")
        print("\t[4] Touristic procedence")
        print("\t[5] Touristic places")
        print("\t[6] Touristic places top 10 contry dist.")
        print("\t[7] Tourist route")
        print("\t[8] Zone route")
        print("\t[0] Exit\n")
    
    def data_analyzed(self):
        os.system('clear')
        print("\nStatistics:")
        n_tweets = self.mongodb_service.tweet_count()
        print("  Tweets analyzed: "+str(n_tweets))
        n_users = self.mongodb_service.users_count()
        print("  Users analyzed: "+str(n_users))
        n_tourists = self.mongodb_service.tourists_count()
        percent_tourists = (float(n_tourists)/float(n_users))*100
        print("  "+str(n_tourists)+" users are tourists "+"(%.2f" % percent_tourists+"%)")
        raw_input("\npress any key to continue...")

    def touristic_distribution(self,display_type):
        n_users = self.mongodb_service.users_count()
        n_tourists = self.mongodb_service.tourists_count()
        n_native = n_users - n_tourists
        percent_tourists = (float(n_tourists)/float(n_users))*100
        percent_natives = (float(n_native)/float(n_users))*100

        percent_values = []
        total_values = []
        alpha_3_labels = []
        labels = []

        countries_counts = self.mongodb_service.countries_counts()
        for l in countries_counts:
            labels.append(l["country"])
            total_values.append(l["count"])
            alpha_3_labels.append(l["alpha_3"])
            percent_l = float(l["count"]) / float(n_tourists) * 100
            percent_values.append(percent_l)
        if display_type == 'percentage':
            plotly.offline.plot({
            "data": [
                {
                "values": [percent_tourists, percent_natives],
                "labels": [
                    "Tourist",
                    "Native"
                ],
                "domain": {"x": [0, .48]},
                "name": "Users",
                "hoverinfo":"label+percent+name",
                "hole": .4,
                "type": "pie"
                },
                {
                "values": percent_values,
                "labels": labels,
                "text":["Tourists"],
                "textposition":"inside",
                "domain": {"x": [.52, 1]},
                "name": "Tourists",
                "hoverinfo":"label+percent+name",
                "hole": .4,
                "type": "pie"
                }],
            "layout": {
                    "title":"Analyzed Users",
                    "annotations": [
                        {
                            "font": {
                                "size": 20
                            },
                            "showarrow": False,
                            "text": "Users",
                            "x": 0.20,
                            "y": 0.5
                        },
                        {
                            "font": {
                                "size": 20
                            },
                            "showarrow": False,
                            "text": "Tourists",
                            "x": 0.8,
                            "y": 0.5
                        }
                    ]
                }
            }, auto_open=True)
        elif display_type == 'total':
            data = [go.Bar(
                    x=labels,
                    y=total_values
            )]
            plotly.offline.plot(data, filename='basic-bar.html')
        elif display_type == 'procedence':
            world_data = [ dict(
                type = 'choropleth',
                locations = alpha_3_labels,
                z = total_values,
                text = labels,
                colorscale = [[0,"rgb(5, 10, 172)"],[0.35,"rgb(40, 60, 190)"],[0.5,"rgb(70, 100, 245)"],\
                    [0.6,"rgb(90, 120, 245)"],[0.7,"rgb(106, 137, 247)"],[1,"rgb(220, 220, 220)"]],
                autocolorscale = False,
                reversescale = True,
                marker = dict(
                    line = dict (
                        color = 'rgb(180,180,180)',
                        width = 0.5
                    ) ),
                colorbar = dict(
                    autotick = False,
                    title = 'Tourists qty'),
            ) ]

            layout = dict(
                title = 'Toourists in Catalonia procedence<br>Source:\
                        <a href="https://www.twitter.com">\
                        Twitter analyzed Data</a>',
                geo = dict(
                    showframe = True,
                    showcoastlines = True,
                    projection = dict(
                        type = 'Mercator'
                    )
                )
            )
            fig = dict( data=world_data, layout=layout )
            plotly.offline.plot( fig, validate=False, filename='d3-world-map.html' )
        raw_input()

    def touristic_places(self):
        cat_tourist_places = self.mongodb_service.catalan_tourist_places()
        lat_list = []
        lon_list = []
        text_list = []
        val_list = []

        for i in cat_tourist_places:
            lat_list.append(i["_id"]["geo"]["lat"])
            lon_list.append(i["_id"]["geo"]["lng"])
            text_list.append(i["_id"]["location"].encode("utf-8")+" Visited times: %.2f" % i["value"])
            val_list.append(i["value"])

        mx = max(val_list)
        mn = min(val_list)
        normalized_values = [((x-mn)/(mx-mn)) for x in val_list]
        adjusted_data = [x*100 if x > 0.01 else 1.0 for x in normalized_values]
        for idx, val in enumerate(adjusted_data ):
            if val > 1:
                size_s = idx+1
                if val > 10:
                    size_m = idx+1
                    if val > 50:
                        size_l = idx+1

        mapbox_access_token = self.MAP_KEY
        end = len(adjusted_data)

        data = [
            go.Scattermapbox(
                lat=lat_list[size_s:end],
                lon=lon_list[size_s:end],
                mode='markers',
                marker=dict(
                    size=10,
                    opacity=0.50,
                    color="blue"
                ),
                text=text_list[size_s:end]
            ),
            go.Scattermapbox(
                lat=lat_list[size_m:size_s+1],
                lon=lon_list[size_m:size_s+1],
                mode='markers',
                marker=dict(
                    size=25,
                    opacity=0.40,
                    color="green"
                ),
                text=text_list[size_m:size_s+1]
            ),
            go.Scattermapbox(
                lat=lat_list[size_l:size_m+1],
                lon=lon_list[size_l:size_m+1],
                mode='markers', 
                marker=dict(
                    size=50,
                    opacity=0.30,
                    color="yellow"
                ),
                text=text_list[size_l:size_m+1]
            ),
            go.Scattermapbox(
                lat=lat_list[0:size_l+1],
                lon=lon_list[0:size_l+1],
                mode='markers',
                marker=dict(
                    size=100,
                    opacity=0.20,
                    color="red"
                ),
                text=text_list[0:size_l+1]
            )
        ]

        layout = go.Layout(
            autosize=True,
            hovermode='closest',
            mapbox=dict(
                accesstoken=mapbox_access_token,
                bearing=0,
                center=dict(
                    lat=41.59,
                    lon=1.52
                ),
                pitch=0,
                zoom=6.5
            ),
        )

        fig = dict(data=data, layout=layout)
        plotly.offline.plot(fig, filename='Multiple Mapbox.html')

    def touristic_places_top(self):
        countries_counts = self.mongodb_service.countries_counts()

        traces = []
        top_ten_countries = countries_counts[:11]
        for country in top_ten_countries:
            country_code = country["alpha_2"]
            country_tourists_counts = self.mongodb_service.get_tourist_places_by_country_code(country_code)
            layouts = []
            values = []
            for count in country_tourists_counts[:20]:
                layouts.append(count["_id"]["location"])
                values.append(count["value"])
            trace = go.Bar(
                x = layouts,
                y = values,
                name = country["country"]
            )
            traces.append(trace)

        data = traces
        layout = go.Layout(
            barmode='stack'
        )

        fig = go.Figure(data=data, layout=layout)
        plotly.offline.plot(fig, filename='stacked-bar')

    def touristic_routes(self,country_code):
        if country_code == 'ALL':
            routes = self.mongodb_service.get_tourist_external_routes()
        else:
            routes = self.mongodb_service.get_tourist_external_routes_by_country_code(country_code)
        max_route_count = 0
        for route in routes:
            for destination in routes[route]["destinations"]:
                if routes[route]["destinations"][destination]["count"] > max_route_count:
                    max_route_count = routes[route]["destinations"][destination]["count"]

        cities = []
        lines = []
        for route in routes:
            city = dict(
                type = 'scattergeo',
                lon = [routes[route]["location"]["geo"]['lng']],
                lat = [routes[route]["location"]["geo"]['lat']],
                hoverinfo = 'text',
                text = [routes[route]['location']["location"].encode("utf-8")],
                mode = 'markers',
                marker = dict( 
                    size=2, 
                    color='rgb(255, 0, 0)',
                    line = dict(
                        width=3,
                        color='rgba(68, 68, 68, 0)'
                    )
                ))
            cities.append(city)
            for destination in routes[route]["destinations"]:
                line = dict(
                    type = 'scattergeo',
                    lon = [ 
                        routes[route]["location"]["geo"]['lng'], 
                        routes[route]["destinations"][destination]["location"]["geo"]['lng']
                        ],
                    lat = [ 
                        routes[route]["location"]["geo"]['lat'], 
                        routes[route]["destinations"][destination]["location"]["geo"]['lat']
                        ],
                    mode = 'lines',
                    line = dict(
                        width = 1,
                        color = 'red',
                    ),
                    opacity = float(routes[route]["destinations"][destination]["count"])/float(max_route_count),
                )
                lines.append(line)

        layout = dict(
                title = 'Routes followed by the tousits that visit Catalonia (Hover for place names)',
                showlegend = False, 
                geo = dict(
                    projection=dict( type='azimuthal equal area' ),
                    showland = True,
                    landcolor = 'rgb(243, 243, 243)',
                    countrycolor = 'rgb(204, 204, 204)',
                ),
            )
        
        fig = dict( data=lines + cities, layout=layout )
        plotly.offline.plot( fig, filename='d3-flight-paths.html' )

    def show_tourist_route(self,screen_name):
        self.AUI.aquire(screen_name)
        routes = self.mongodb_service.get_tourist_routes(screen_name)
        cities = []
        lines = []
        for route in routes:
            city = dict(
                type = 'scattergeo',
                lon = [routes[route]["location"]["geo"]['lng']],
                lat = [routes[route]["location"]["geo"]['lat']],
                hoverinfo = 'text',
                text = [routes[route]['location']["location"].encode("utf-8")],
                mode = 'markers',
                marker = dict( 
                    size=2, 
                    color='rgb(255, 0, 0)',
                    line = dict(
                        width=3,
                        color='rgba(68, 68, 68, 0)'
                    )
                ))
            cities.append(city)
            for destination in routes[route]["destinations"]:
                line = dict(
                    type = 'scattergeo',
                    lon = [ 
                        routes[route]["location"]["geo"]['lng'], 
                        routes[route]["destinations"][destination]["location"]["geo"]['lng']
                        ],
                    lat = [ 
                        routes[route]["location"]["geo"]['lat'], 
                        routes[route]["destinations"][destination]["location"]["geo"]['lat']
                        ],
                    mode = 'lines',
                    line = dict(
                        width = 1,
                        color = 'red',
                    )
                )
                lines.append(line)

        layout = dict(
                title = 'Routes followed by the tousits that visit Catalonia (Hover for place names)',
                showlegend = False, 
                geo = dict(
                    projection=dict( type='azimuthal equal area' ),
                    showland = True,
                    landcolor = 'rgb(243, 243, 243)',
                    countrycolor = 'rgb(204, 204, 204)',
                ),
            )
        
        fig = dict( data=lines + cities, layout=layout )
        plotly.offline.plot( fig, filename='d3-flight-paths.html' )

def main():
    tis = TourismInformationSystem()
    tis.run()    

if __name__ == '__main__':
    main()
    #if len(sys.argv) > 1:
    #show_tourist_route(sys.argv[1])
        