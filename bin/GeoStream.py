import DataProcessor

#Main classe responsible for starting the Stream and filtering the geolocated data
def main():
    processor = DataProcessor.DataProcessor()
    processor.start_streaming()

if __name__ == '__main__':
    main()