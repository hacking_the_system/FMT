import tweepy
import json
import time
import configparser
import TwitterService
import GeoListener
import Processor
from pymongo.errors import NetworkTimeout
try:
    from urllib3 import ProtocolError  # Python 2.X
except ImportError:
    from urllib3.exceptions import ProtocolError  # Python 3
    from urllib3.exceptions import ReadTimeoutError  # Python 3

class DataProcessor:
    def __init__(self):
        self.config = configparser.ConfigParser()
        self.config.read("config.ini")
        self.locations = json.loads(self.config['Geo_Boxes']['catalunya'])
        self.tw_service = TwitterService.TwitterService()
        self.processor = Processor.Processor()

    def get_locations(self):
        return self.locations

    def get_stream(self):
        stream = self.tw_service.get_tw_stream(self.processor.process_data)
        return stream
    
    def analyze_user(self,screen_name):
        user = self.processor.analyze_user(screen_name)
        if user is not None:
            #print(user["origin"]["location"])
            pass

    def start_streaming(self):
        stream = self.get_stream()
        while True:
            try:
                stream.filter(locations=self.get_locations())
            except NetworkTimeout:
                print("Conncection with the database broken.")
                print("Reconecting...")
            except ReadTimeoutError:
                print("Conncection broken.")
                print("Reconecting...")
            except tweepy.error.TweepError:
                print("Conncection broken.")
                print("Reconecting...")
            except ProtocolError:
                print("Conncection broken.")
                print("Reconecting...")