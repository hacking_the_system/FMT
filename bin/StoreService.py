import FileStoreService
import MongoDbService
import json
import configparser

class StoreService:
    def __init__(self):
        self.fs_service = FileStoreService.FileStoreService()
        self.mongodb_service = MongoDbService.MongoDbService()
        config = configparser.ConfigParser()
        config.read("config.ini")
        self.parameters =  config["Storage"]

    def store_tweet(self,tweet):
        self.fs_service.plain_text_storage('tweet',tweet)
        data = json.loads(tweet)
        if self.exist_tweet(data["id_str"]) == False:
            if self.parameters["type"] == 'light':
                light_data = {}
                light_data["id_str"] = data["id_str"]
                self.mongodb_service.store_tweet(light_data)        
            else:
                self.mongodb_service.store_tweet(data)        

    def store_plain_text_user(self,user):
        str_user = str(user)+'\n'
        self.fs_service.plain_text_storage('user',str_user)

    def store_user(self,user):
        if self.parameters["type"] == 'light':
            light_user = {}
            light_user["screen_name"] = user["screen_name"]
            light_user["origin"] = user["origin"]
            light_user["data_determined"] = user["data_determined"]
            self.mongodb_service.store_user(light_user)        
        else:
            self.mongodb_service.store_user(user)
        
    
    def store_location(self,location):
        self.mongodb_service.store_location(location)

    def get_location(self,location):
        return self.mongodb_service.get_location(location)
        
    def get_user(self,username):
        return self.mongodb_service.get_user(username)

    def exist_user(self,username):
        return self.mongodb_service.exist_user(username)

    def exist_location(self,location):
        return self.mongodb_service.exist_location(location)
    
    def exist_tweet(self,tweet):
        return self.mongodb_service.exist_tweet(tweet)

    def remove_user(self,username):
        self.mongodb_service.remove_user(username)
    
    def exist_tourist(self,username):
        return self.mongodb_service.exist_tourist(username)

    def remove_tourist(self,username):
        self.mongodb_service.remove_tourist(username)
    
    def store_tourist(self,tourist):
        self.mongodb_service.store_tourist(tourist)
        