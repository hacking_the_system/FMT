import pycountry
import Utils
import json
import StoreService
import GoogleService
import TwitterService
import TweetService
import LocationService
import UserService
import TouristService

class Processor:
    def __init__(self):
        self.tw_service = TwitterService.TwitterService()
        self.st_service = StoreService.StoreService()
        self.gg_service = GoogleService.GoogleService()
        self.location_service = LocationService.LocationService()
        self.tweet_service = TweetService.TweetService()
        self.user_service = UserService.UserService()
        self.tourist_service = TouristService.TouristService()
        
    def show_tweet_info(self,tweet):
        username = tweet['user']['screen_name']
        print("Acquired tweet from:")
        print("  @"+username)
        #print("  "+tweet_dict['text'])
        print("  located in: "+tweet["place"]["full_name"])

    def is_valid_data(self,data):
        if data["place"] is None:
            print("Weird case:")
            print(data)        
            return False
        location = self.location_service.get_location_by_stream_data(data)
        if location is None:
            return False
        if location["country_code"] != 'ES':
            print("Data not from Catalonia.")
            return False
        self.show_tweet_info(data)
        print("  determined tweet location: "+location["address"])
        return True

    def analyze_user(self,screen_name):
        user = self.user_service.get_user_by_screen_name(screen_name)
        if user["origin"] is not None:
            print("  Determined user location: "+user["origin"]["address"])
            tourist = not user["origin"]["catalunya"]
            if tourist:
                print("  This user is a tourist")
                self.tourist_service.analyze_tourist(user)
            else:
                print("  User not a tourist")
        else:
            print("  Unable to determine location")
        return user

        
    def process_data(self,data):
        json_data = json.loads(data)
        valid = self.is_valid_data(json_data)
        if valid:
            self.tweet_service.store_tweet(data)
            screen_name = json_data["user"]["screen_name"]
            self.analyze_user(screen_name)
        else:
            print("Invalid data.")
            print("Discarding data...")