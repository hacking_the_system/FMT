import configparser
import json

class LocationObj:
    def __init__(self):
        self.data = {}
        config = configparser.ConfigParser()
        config.read("config.ini")
        self.box = json.loads(config['Geo_Boxes']['catalunya'])

    def validate_twitter_data(self,data):
        if(
            data.country is None
            or data.country_code is None
            or data.centroid[1] is None
            or data.centroid[0] is None
            or data.full_name is None
        ):
            print(data)
            raise Exception

    def set_twitter_data(self,data):
        self.validate_twitter_data(data)
        self.data["country"] = data.country
        self.data["country_code"] = data.country_code
        geo = {}
        geo["lat"] = data.centroid[1]
        geo["lng"] = data.centroid[0]
        self.data["geo"] = geo
        self.data["address"] = data.full_name
        self.data["location"] = data.full_name
        self.data["catalunya"] = self.is_twitter_place_inside_cat(data)

    def is_twitter_place_inside_cat(self,place):
        if place.country_code != 'ES':
            return False
        if(
            place.centroid[0] < self.box[0]
            or place.centroid[0] > self.box[2]
            or place.centroid[1] < self.box[1]
            or place.centroid[1] > self.box[3]
        ):
            return False
        return True

    def get_country_component_from_google_address(self,address_components):
        for component in address_components:
            if component["types"][0] == 'country':
                return component
        return []

    def is_catalunya_google_address(self,address_components):
        is_cat = False
        for component in address_components:
            if (
                component["types"][0] == 'administrative_area_level_1'
                and component["short_name"] == 'CT'
                and self.data["country_code"] == 'ES'
            ):
                is_cat = True
        return is_cat

    def validate_google_data(self,location,data,country_component):
        if( 
            "formatted_address" not in data
            or "short_name" not in country_component
            or "long_name" not in country_component
            or "geometry" not in data
            or "location" not in data["geometry"]
        ):
            print(location)
            print(data)
            raise Exception

    def set_google_data(self,location,data):
        country_component = self.get_country_component_from_google_address(data["address_components"])
        self.validate_google_data(location,data,country_component)
        self.data["address"] = data["formatted_address"]
        self.data["country_code"] = country_component["short_name"]
        self.data["country"] = country_component["long_name"]
        self.data["catalunya"] = self.is_catalunya_google_address(data["address_components"])
        self.data["geo"] = data["geometry"]["location"]
        self.data["location"] = location

    def validate_country_data(self,country,country_code,data):
        if( 
            "formatted_address" not in data
            or "geometry" not in data
            or "location" not in data["geometry"]
        ):
            print(country)
            print(country_code)
            print(data)
            raise Exception

    def set_country_data(self,country,country_code,data):
        self.validate_country_data(country,country_code,data)
        self.data["address"] = data["formatted_address"]
        self.data["country_code"] = country_code
        self.data["country"] = country
        self.data["catalunya"] = self.is_catalunya_google_address(data["address_components"])
        self.data["geo"] = data["geometry"]["location"]
        self.data["location"] = country

    def get_location_object(self):
        return self.data