import operator

def get_key_from_max_val(d):
    return max(d.items(), key=operator.itemgetter(1))[0]