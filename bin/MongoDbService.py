import configparser
import pymongo
import pycountry
from operator import itemgetter
from bson.son import SON
from bson.code import Code
import json
try:
    from urllib import quote_plus  # Python 2.X
except ImportError:
    from urllib.parse import quote_plus  # Python 3+

class MongoDbService:
    def __init__(self):
        config = configparser.ConfigParser()
        config.read("config.ini")
        self.parameters =  config["MongoDB"]

    def get_uri(self):
        user = self.parameters["user"]
        password = self.parameters["password"]
        host = self.parameters["host"]
        port = self.parameters["port"]
        uri = "mongodb://%s:%s@%s:%s/fmt" % (quote_plus(user),quote_plus(password),host,port)
        return uri

    def get_database(self,database):
        uri = self.get_uri()
        client = pymongo.MongoClient(uri)
        db = client[database]
        return db

    def get_collection(self,collection):
        db = self.get_database(self.parameters["db_name"])
        tweets = db[collection]
        return tweets

    def store_tweet(self,tweet):
        tweets = self.get_collection("tweets")
        tweets.insert_one(tweet)
        
    def exist_tweet(self,tweet_id):
        tweets = self.get_collection("tweets")
        if tweets.find_one({'id_str':tweet_id}) is not None:
            return True
        return False

    def store_user(self,user):
        users = self.get_collection("users")
        users.insert_one(user)

    def store_location(self,location):
        locations = self.get_collection("locations")
        locations.insert_one(location)

    def exist_user(self,username):
        users = self.get_collection("users")
        if users.find_one({'screen_name':username}) is not None:
            return True
        return False

    def exist_location(self,location):
        locations = self.get_collection("locations")
        if locations.find_one({'location':location}) is not None:
            return True
        return False

    def get_location(self,location):
        locations = self.get_collection("locations")
        return locations.find_one({'location':location})

    def get_user(self,username):
        users = self.get_collection("users")
        return users.find_one({'screen_name':username})
    
    def remove_user(self,username):
        users = self.get_collection("users")
        return users.delete_one({'screen_name':username})

    def store_tourist(self,tourist):
        tourists = self.get_collection("tourists")
        tourists.insert_one(tourist)

    def exist_tourist(self,username):
        tourists = self.get_collection("tourists")
        if tourists.find_one({'username':username}) is not None:
            return True
        return False
    
    def remove_tourist(self,username):
        tourists = self.get_collection("tourists")
        tourists.delete_one({'username':username})

    def tweet_count(self):
        tweets = self.get_collection("tweets")
        return tweets.count()

    def users_count(self):
        users = self.get_collection("users")
        return users.count()

    def tourists_count(self):
        tourists = self.get_collection("tourists")
        return tourists.find({"destinations":{"$ne":[]}}).count()

    def countries_counts(self):
        tourists = self.get_collection("tourists")
        query = tourists.aggregate([
            {"$match":{"destinations":{"$ne":[]}}},
            {"$group" : {"_id":"$origin.country_code", "count":{"$sum":1}}},
            {"$sort":{"count":-1}}
        ])
        result = []
        country_names = {country.alpha_2: country.name for country in pycountry.countries}
        alpha_3 = {country.alpha_2: country.alpha_3 for country in pycountry.countries}
        for row in query:
            r = {}
            r["country"] = country_names.get(row["_id"])
            r["alpha_3"] = alpha_3.get(row["_id"])
            r["alpha_2"] = row["_id"]
            r["count"] = row["count"]
            result.append(r)
        return(result)

    def catalan_tourist_places(self):
        tourists = self.get_collection("tourists")
        map = Code("function () {"
            "  this.destinations.forEach(function(z) {"
            "    if(z.location.catalunya == true) {"
            "      emit(z.location.location, 1);"
            "    }"
            "  });"
            "}")
        reduce = Code("function (key, values) {"
            "  return Array.sum(values);"
            "}")
        result = tourists.map_reduce(map, reduce, "myresults")
        result_list = []
        cat_locations = {}
        query = self.get_collection("locations").find({"catalunya":True})
        for r in query:
            cat_locations[r["location"]] = r["geo"]
        for doc in result.find().sort([("value", -1)]):
            result = {}
            result["value"] = doc["value"]
            result["_id"] = {}
            result["_id"]["location"] = doc["_id"]
            result["_id"]["geo"] = cat_locations[doc["_id"]]
            result_list.append(result)
        return(result_list)

    def get_tourist_routes(self,screen_name):
        tourists = self.get_collection("tourists").find({"destinations":{"$ne":[]},"username":screen_name})
        routes = {}
        for tourist in tourists:
            start_name = tourist["origin"]["location"]
            start_loc = tourist["origin"]
            destinations = sorted(tourist["destinations"], key=itemgetter('time')) 
            for destination in destinations:
                end_name = destination["location"]["location"]
                end_loc = destination["location"]
                if start_name not in routes:
                    routes[start_name] = {}
                    routes[start_name]["location"] = start_loc
                    routes[start_name]["destinations"] = {}
                if end_name not in routes[start_name]["destinations"]:
                    routes[start_name]["destinations"][end_name] = {}
                    routes[start_name]["destinations"][end_name]["location"] = end_loc
                    routes[start_name]["destinations"][end_name]["count"] = 1
                else:
                    routes[start_name]["destinations"][end_name]["count"] += 1
                start_name = end_name
                start_loc = end_loc
        return routes

    def get_tourist_external_routes(self):
        tourists = self.get_collection("tourists").find({"destinations":{"$ne":[]}})
        routes = {}
        for tourist in tourists:
            start_name = tourist["origin"]["location"]
            start_loc = tourist["origin"]
            destinations = sorted(tourist["destinations"], key=itemgetter('time'))
            filtered_destinations = [destination for destination in destinations if destination["location"]["catalunya"]==False]
            for destination in filtered_destinations:
                end_name = destination["location"]["location"]
                end_loc = destination["location"]
                if start_name not in routes:
                    routes[start_name] = {}
                    routes[start_name]["location"] = start_loc
                    routes[start_name]["destinations"] = {}
                if end_name not in routes[start_name]["destinations"]:
                    routes[start_name]["destinations"][end_name] = {}
                    routes[start_name]["destinations"][end_name]["location"] = end_loc
                    routes[start_name]["destinations"][end_name]["count"] = 1
                else:
                    routes[start_name]["destinations"][end_name]["count"] += 1
                start_name = end_name
                start_loc = end_loc
        return routes

    def get_tourist_external_routes_by_country_code(self,country_code):
        tourists = self.get_collection("tourists").find({"destinations":{"$ne":[]},"origin.country_code":country_code})
        routes = {}
        for tourist in tourists:
            start_name = tourist["origin"]["location"]
            start_loc = tourist["origin"]
            destinations = sorted(tourist["destinations"], key=itemgetter('time'))
            filtered_destinations = [destination for destination in destinations if destination["location"]["catalunya"]==False]
            for destination in filtered_destinations:
                end_name = destination["location"]["location"]
                end_loc = destination["location"]
                if start_name not in routes:
                    routes[start_name] = {}
                    routes[start_name]["location"] = start_loc
                    routes[start_name]["destinations"] = {}
                if end_name not in routes[start_name]["destinations"]:
                    routes[start_name]["destinations"][end_name] = {}
                    routes[start_name]["destinations"][end_name]["location"] = end_loc
                    routes[start_name]["destinations"][end_name]["count"] = 1
                else:
                    routes[start_name]["destinations"][end_name]["count"] += 1
                start_name = end_name
                start_loc = end_loc
        return routes

    def get_tourist_places_by_country_code(self,country_code):
        tourists = self.get_collection("tourists")
        map = Code("function () {"
            "  if(this.origin.country_code == '"+country_code+"'){"
            "    this.destinations.forEach(function(z) {"
            "      if(z.location.catalunya == true) {"
            "        emit(z.location.location, 1);"
            "      }"
            "    });"
            "  }"
            "}")
        reduce = Code("function (key, values) {"
            "  return Array.sum(values);"
            "}")
        result = tourists.map_reduce(map, reduce, "myresults")
        result_list = []
        cat_locations = {}
        query = self.get_collection("locations").find({"catalunya":True})
        for r in query:
            cat_locations[r["location"]] = r["geo"]
        for doc in result.find().sort([("value", -1)]):
            result = {}
            result["value"] = doc["value"]
            result["_id"] = {}
            result["_id"]["location"] = doc["_id"]
            result["_id"]["geo"] = cat_locations[doc["_id"]]
            result_list.append(result)
        return(result_list)