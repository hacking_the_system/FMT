import tweepy

# This is the listener, resposible for receiving data
class GeoListener(tweepy.StreamListener):
    def __init__(self,process_data):
        self.process_data = process_data

    def on_data(self, data):
        self.process_data(data)
        return True

    def on_error(self, status_code):
        print(status_code)
        if status_code == 420:
            #returning False in on_error disconnects the stream
            return False
        # returning non-False reconnects the stream, with backoff.
        return True