import tweepy
import json
import configparser
import GeoListener
import time
try:
    from urllib import quote_plus  # Python 2.X
except ImportError:
    from urllib.parse import quote_plus  # Python 3+import tweepy

import LocationObj

class TwitterService:
    def __init__(self):
        config = configparser.ConfigParser()
        config.read("config.ini")
        self.credentials = config["Twitter_API"]

    def get_twitter_auth(self):
        consumer_key = self.credentials['consumer_key']
        consumer_secret = self.credentials['consumer_secret']
        access_token = self.credentials['access_token']
        access_token_secret = self.credentials['access_token_secret']
        auth = tweepy.OAuthHandler(consumer_key, consumer_secret)
        auth.set_access_token(access_token, access_token_secret)
        return auth

    def get_twitter_api(self):
        auth = self.get_twitter_auth()
        return tweepy.API(auth_handler=auth,wait_on_rate_limit=True,wait_on_rate_limit_notify=True)

    def get_tw_stream(self,process_data):
        listener = GeoListener.GeoListener(process_data)
        return tweepy.Stream(self.get_twitter_auth(), listener)

    def get_user_by_screen_name(self,screen_name):
        api = self.get_twitter_api()
        user = api.get_user(screen_name)
        return user

    def get_twitter_place_by_id(self,id):
        api = self.get_twitter_api()
        try:
            place = api.geo_id(id)
            return place
        except tweepy.error.TweepError:
            return None

    def create_location_from_twitter_place(self,place):
        loc_obj = LocationObj.LocationObj()
        loc_obj.set_twitter_data(place)
        return loc_obj.get_location_object()

    def get_location_from_twitter_id(self, tw_id):
        location = None
        place = self.get_twitter_place_by_id(tw_id)
        if place is not None:
            if place.country_code != '':
                location = self.create_location_from_twitter_place(place)
        return location

    def get_tweets_by_screen_name(self,screen_name):
        api = self.get_twitter_api()
        statuses = api.user_timeline(screen_name=screen_name,count=3200)
        return statuses

    def geolocate(self,location):
        api = self.get_twitter_api()
        utf8_loc = location.encode('utf-8')
        url_loc = quote_plus(utf8_loc)
        data = api.geo_search(query=url_loc)
        if len(data)>0:
            #TODO Comprovar correctesa ja que agafem el primer element
            if data[0].country_code != '':
                place = self.create_location_from_twitter_place(data[0])
                return place
        return None