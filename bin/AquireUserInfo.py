import DataProcessor
import sys

#Main classe responsible for starting the Stream and filtering the geolocated data
class AquireUserInfo:
    def aquire(self,screen_name):
        processor = DataProcessor.DataProcessor()
        processor.analyze_user(screen_name)

if __name__ == '__main__':
    if len(sys.argv) > 1:
        AUI = AquireUserInfo()
        AUI.aquire(sys.argv[1])
    else:
        print("usage: AquireUserInfo <screen_name>")
    