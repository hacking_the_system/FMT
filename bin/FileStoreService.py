import configparser
import datetime
import os

class FileStoreService:
    def __init__(self):
        config = configparser.ConfigParser()
        config.read("config.ini")
        self.system_paths = config["File_Storage"]

    def get_file_name(self,obj_type):
        apendix = datetime.datetime.now().strftime('_%Y%m%d_%H:00.txt')
        filename = obj_type+apendix
        return filename

    def get_store_path(self,obj_type):
        return self.system_paths[obj_type]
    
    def write_data(self,filepath,data):
        file = open(filepath,'a')
        file.write(data)
        file.close()

    def plain_text_storage(self,obj_type,data):
        path = self.get_store_path(obj_type)
        if not os.path.exists(path):
            os.makedirs(path)
        filename = self.get_file_name(obj_type)
        filepath = path+filename
        self.write_data(filepath,data)